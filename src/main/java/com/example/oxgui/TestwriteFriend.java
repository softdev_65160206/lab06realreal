/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.oxgui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class TestwriteFriend {
    public static void main(String[] args)  {
        ObjectOutputStream oos = null;
        try {
            Friend f1 = new Friend("Worawit",44,"0949195955");
            Friend f2 = new Friend("Jakkaman",48,"0949195195");
            System.out.println(f1);
            System.out.println(f2);
             File file = new File("friends.dat");
            FileOutputStream fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(f1);
            oos.writeObject(f2);
            oos.close();
            fos.close();
        } catch (IOException ex) {
            Logger.getLogger(TestwriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                oos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestwriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
}
